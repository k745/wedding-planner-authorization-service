import {Datastore} from '@google-cloud/datastore'
import express from 'express'
import cors from 'cors'
import { MissingResourceError } from './errors.js';

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore();

app.patch('/users/login', async (req, res) =>{
    const query = datastore.createQuery('Employee').filter('email',"=",req.body.email).filter('password','=',req.body.password);
    try{
        const [data,metaInfo] = await datastore.runQuery(query)
        if(data){
            var user = {
                fName: data[0].fName,
                lName: data[0].lName
            }
            res.status(200);
            res.send(user);
        } else{
            res.status(404)
            res.send("Unsuccessful login attempt")    
        }
    } catch {
        res.status(404)
        res.send("Unsuccessful login attempt")   
    }
});

app.get('/users/:email/verify', async (req, res)=>{

    const key = datastore.key(['Employee',req.params.email]);
    const [data,metaInfo] = await datastore.get(key);
    if(!data){
        res.status(404)
        res.send("The email address you entered could not be found")
    } else {
        res.status(200)
        res.send(data)
    }
});

app.post('/employees', async (req, res) =>{
    const employee = req.body;
    const key = datastore.key(['Employee',employee.email])
    const response = await datastore.save({key:key,data:employee})
    res.status(201)
    res.send('Created new employee')
})

const PORT = process.env.PORT || 3000;
app.listen(PORT,()=>console.log(`Application Started on port ${PORT}`))