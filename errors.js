export class MissingResourceError{

    message;
    description = "Sorry, the resource you requested could not be found";

    constructor(message){
        this.message = message;
    }
}

